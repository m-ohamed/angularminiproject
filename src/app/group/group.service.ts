import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private http: HttpClient, private userService: UserService) { }

  getHeaders() {
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(this.userService.loggedUsername + ":" + this.userService.loggedPassword)
        })
    };

    return httpOptions;
  }

  getGroup(groupId: number) {
    return this.http.get('http://localhost:8880/group/find/' + groupId, this.getHeaders());
  }

  createGroup(ownerUsername: string, groupName: string) {
    return this.http.post('http://localhost:8880/group/create',
      {
        'ownerUsername': ownerUsername,
        'groupName': groupName
      },
      this.getHeaders()).subscribe(data => {
        console.log(data);
      });
  }

  updateGroup(groupId: number, ownerUid: number, groupName: string) {
    console.log(groupId + " " + ownerUid + " " + groupName);
    if (ownerUid !== undefined && ownerUid !== null) 
      return this.http.put('http://localhost:8880/group/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.userService.loggedUsername + ":" + this.userService.loggedPassword)
          },
          params: {
            'groupId': ''+groupId,
            'groupOwner': ''+ownerUid
          }
        }).subscribe(data => {
          console.log(data);
        });

    if (groupName !== undefined || groupName.replace(' ','') !== '')
      return this.http.put('http://localhost:8880/group/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.userService.loggedUsername + ":" + this.userService.loggedPassword)
          },
          params: {
            'groupId': '' + groupId,
            'groupName': groupName
          }
        }).subscribe(data => {
          console.log(data);
        });
  }

  deleteGroup(groupId: number)
  {
    return this.http.delete('http://localhost:8880/group/delete/' + groupId, this.getHeaders()).subscribe(data => {
      console.log(data);
    });
  }


}
