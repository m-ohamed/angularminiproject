import { Component, OnInit } from '@angular/core';
import { GroupService } from './group.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  group;

  constructor(private groupService: GroupService) { }

  ngOnInit() {
  }

  getGroup(groupId: number)
  {
    this.groupService.getGroup(groupId).subscribe(data => {
      this.group = data;
      console.log(data);
      console.log("new line");
      console.log(data['users'][0]['username']);
      // return this.users;
    });
  }

}
