import { Component } from '@angular/core';
import { UserService } from './user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'UserManagement';
  loadedFeature = 'recipe';
  isLoggedIn = this.userService.user === undefined ? false : true;
  role;

  constructor(private userService: UserService){}

  onNavigate(feature: string)
  {
    this.isLoggedIn = this.userService.user === undefined ? false : true;
    this.role = this.userService.user === undefined ? undefined : this.userService.user['role'];

    this.loadedFeature = feature;
  }
}
