import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../user/user.service';
// import { UserModule } from '../shared/user.module';
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/map';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  users;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  getData(username: string, password: string)
  {
    // console.log("username: " + username + " Password: " + password);
    this.userService.userLogin(username, password).subscribe(data => {
      
      this.userService.setUserInfo(data);
      // this.userService.setLoggedRole(data['role']);
      // this.userService.setLoggedId(data['id']);
      // console.log(this.userService.getUser());
      // this.userService.setUserInfo(data['id'],data['username'],data['firstName'],data['lastName'],data['email'],data['role']);
      // console.log(this.userService.getLoggedRole());
      // console.log(this.userService.getUser());
      //return this.users;
    });

    // console.log("2nd log: " + this.userService.getLoggedRole());
    
    
  }

}
