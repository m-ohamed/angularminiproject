import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() featureSelected = new EventEmitter<string>();
  role;


  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  onSelect(feature: string)
  {
    if(feature === 'logout')
    {
      this.userService.user = undefined;
      this.userService.loggedUsername = undefined;
      this.userService.loggedRole = undefined;
      this.userService.loggedPassword = undefined;
    }

    this.role = this.userService.user === undefined ? undefined : this.userService.user['role'];
    this.featureSelected.emit(feature);
  }

}
