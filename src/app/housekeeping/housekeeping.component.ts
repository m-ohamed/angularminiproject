import { Component, OnInit } from '@angular/core';
import { GroupService } from '../group/group.service';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-housekeeping',
  templateUrl: './housekeeping.component.html',
  styleUrls: ['./housekeeping.component.css']
})
export class HousekeepingComponent implements OnInit {

  constructor(private groupService: GroupService, private userService: UserService) { }

  ngOnInit() {
  }

  createGroup(ownerUsername: string, groupName: string) {
    this.groupService.createGroup(ownerUsername, groupName);
  }

  updateGroup(groupId: number, ownerUid, groupName: string) {
    if (ownerUid.value.length < 1)
      ownerUid = undefined;

    this.groupService.updateGroup(groupId, ownerUid.value, groupName);
  }

  deleteGroup(groupId: number) {
    this.groupService.deleteGroup(groupId);
  }

  createUser(username: string, firstName: string, lastName: string, email: string, password: string, role: string) {
    this.userService.createUser(username, firstName, lastName, email, password, role);
  }

  updateUser(username, firstName, lastName, email, password, newPassword, role) {
    if (firstName.length < 1)
      firstName = undefined;

    if (lastName.length < 1)
      lastName = undefined;

    if (email.length < 1)
      email = undefined;

    if (password.length < 1)
      password = undefined;

    if (newPassword.length < 1)
      newPassword = undefined;

    if (role.length < 1)
      role = undefined;

    this.userService.updateUser(username.value, firstName.value, lastName.value, email.value, password.value, newPassword.value, role.value);
  }

  moveUser(username: string, oldGroupId: number, newGroupId: number) {
    this.userService.moveUser(username, oldGroupId, newGroupId);
  }

  addUser(username: string, groupId: number) {
    this.userService.addUser(username, groupId);
  }

  removeUser(username: string, groupId: number) {
    this.userService.removeUser(username, groupId);
  }

  restoreUser(username: string) {
    this.userService.restoreUser(username);
  }

  deleteUser(username: string) {
    this.userService.deleteUser(username);
  }

}
