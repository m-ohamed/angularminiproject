import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UserModule { 

  constructor(private id: number, private username: string, private firstName: string, private lastName: string, private email: string, private role: string){}

  setId(id: number)
  {
    this.id = id;
  }

  setUsername(username: string)
  {
    this.username = username;
  }

  setFirstName(firstName: string)
  {
    this.firstName = firstName;
  }

  setLastName(lastName: string)
  {
    this.lastName = lastName;
  }

  setEmail(email: string)
  {
    this.email = email;
  }

  setRole(role: string)
  {
    this.role = role;
  }

  getUsername()
  {
    return this.username;
  }

  getId()
  {
    return this.id;
  }

  getFirstName()
  {
    return this.firstName;
  }

  getLastName()
  {
    return this.lastName;
  }

  getEmail()
  {
    return this.email;
  }

  getRole()
  {
    return this.role;
  }
}
