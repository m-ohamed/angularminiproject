import { Component, OnInit } from '@angular/core';
import { UserModule } from '../shared/user.module';
import { UserService } from './user.service';
// import { userInfo } from 'os';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users;
  selectedU;
  role;// = 'admin';

  constructor(private userService: UserService) { }

  ngOnInit() {
    console.log(this.role);
  }

  getAllUsers()
  {
    this.role = this.userService.user === undefined ? undefined : this.userService.user['role'];
    this.userService.getAllUsers().subscribe(data => {
      this.users = data;
      console.log(data);
      // return this.users;
    });
  }

  updateUser()
  {
    // this.userService.updateUserInformation("user","2.0",undefined,undefined,undefined,undefined,undefined).subscribe();
  }

  selectedUser(selection)
  {
    this.selectedU = selection;
  }
}
