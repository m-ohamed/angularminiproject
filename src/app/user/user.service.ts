import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModule } from '../shared/user.module';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users;
  user;
  loggedUsername: string;
  loggedPassword: string;
  loggedRole: string;

  constructor(private http: HttpClient) { }

  getHeaders() {
    const httpOptions = {
      headers: new HttpHeaders(
        {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
        })
    };

    return httpOptions;
  }

  setUserInfo(data: any)//id: number, username: string, firstName: string, lastName: string, email: string, role: string)
  {
    this.user = data;
    // this.user.setId(id);
    // this.user.setUsername(username);
    // this.user.setFirstName(firstName);
    // this.user.setLastName(lastName);
    // this.user.setEmail(email);
    // this.user.setRole(role);

  }

  getUser() {
    return this.user;
  }

  userLogin(username: string, password: string) {
    this.loggedUsername = username;
    this.loggedPassword = password;
    // console.log(this.getLoggedUsername());
    return this.http.get('http://localhost:8880/user/login/' + this.loggedUsername, this.getHeaders());

    // return this.http.get('http://localhost:8880/user/login', this.getHeaders());
  }

  getAllUsers() {
    return this.http.get('http://localhost:8880/user/getAll', this.getHeaders());
  }

  updateUser(username: string, firstName: string, lastName: string, email: string, currentPassword: string, newPassword: string, role: string) {

    if (firstName !== undefined)
      return this.http.put('http://localhost:8880/user/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
          },
          params: {
            'username': username,
            'firstName': firstName
          }
        }).subscribe(data => {
          console.log(data);
        });

    if (lastName !== undefined)
      return this.http.put('http://localhost:8880/user/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
          },
          params: {
            'username': username,
            'lastName': lastName
          }
        }).subscribe(data => {
          console.log(data);
        });

    if (email !== undefined)
      return this.http.put('http://localhost:8880/user/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
          },
          params: {
            'username': username,
            'email': email
          }
        }).subscribe(data => {
          console.log(data);
        });

    if (currentPassword !== undefined && newPassword !== undefined)
      return this.http.put('http://localhost:8880/user/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
          },
          params: {
            'username': username,
            'currentPassword': currentPassword,
            'newPassword': newPassword
          }
        }).subscribe(data => {
          console.log(data);
        });

    if (role !== undefined)
      return this.http.put('http://localhost:8880/user/update', {},
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
          },
          params: {
            'username': username,
            'role': role
          }
        }).subscribe(data => {
          console.log(data);
        });
  }

  createUser(username: string, firstName: string, lastName: string, email: string, password: string, role: string) {
    return this.http.post('http://localhost:8880/user/create',
      {
        'username': username,
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'password': password,
        'role': role
      },
      this.getHeaders()).subscribe(data => {
        console.log(data);
      });
  }

  // updateUser(username: string, firstName: string, lastName: string, email: string, password: string, newPassword: string, role: string)
  // {
  //   this.userService.updateUser(username, firstName, lastName, email, password, newPassword, role);
  // }

  moveUser(username: string, oldGroupId: number, newGroupId: number) {
    return this.http.put('http://localhost:8880/user/move', {},
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
        },
        params: {
          'username': username,
          'oldGroupId': '' + oldGroupId,
          'newGroupId': '' + newGroupId
        }
      }).subscribe(data => {
        console.log(data);
      });
  }

  addUser(username: string, groupId: number) {
    return this.http.put('http://localhost:8880/user/addUser', {},
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
        },
        params: {
          'username': username,
          'groupId': '' + groupId
        }
      }).subscribe(data => {
        console.log(data);
      });
  }

  removeUser(username: string, groupId: number) {
    return this.http.delete('http://localhost:8880/user/removeUser',
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + btoa(this.loggedUsername + ":" + this.loggedPassword)
        },
        params: {
          'username': username,
          'groupId': '' + groupId
        }
      }).subscribe(data => {
        console.log(data);
      });
  }

  restoreUser(username: string) {
    return this.http.put('http://localhost:8880/user/restore/' + username, {}, this.getHeaders()).subscribe(data => {
      console.log(data);
    });
  }

  deleteUser(username: string) {
    return this.http.delete('http://localhost:8880/user/delete/' + username, this.getHeaders()).subscribe(data => {
      console.log(data);
    });
  }

  setLoggedUsername(newUsername: string) {
    this.loggedUsername = newUsername;
  }

  setLoggedPassword(newPassword: string) {
    this.loggedPassword = newPassword;
  }

  setLoggedRole(newRole: string) {
    this.loggedRole = newRole;
  }

  getLoggedUsername() {
    return this.loggedUsername;
  }

  getLoggedPassword() {
    return this.loggedPassword;
  }

  getLoggedRole() {
    return this.loggedRole;
  }
}
