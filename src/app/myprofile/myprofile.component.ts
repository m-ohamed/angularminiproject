import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  updateUser(firstName, lastName, email, password, newPassword, role) {
    if (firstName.length < 1)
      firstName = undefined;

    if (lastName.length < 1)
      lastName = undefined;

    if (email.length < 1)
      email = undefined;

    if (password.length < 1)
      password = undefined;

    if (newPassword.length < 1)
      newPassword = undefined;

    if (role.length < 1)
      role = undefined;

    this.userService.updateUser(this.userService.getLoggedUsername(), firstName.value, lastName.value, email.value, password.value, newPassword.value, role.value);
  }

}
