import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { UserComponent } from './user/user.component';
import { MyprofileComponent } from './myprofile/myprofile.component';
import { GroupComponent } from './group/group.component';
import { HousekeepingComponent } from './housekeeping/housekeeping.component';

const appRoutes: Routes = [
  { path: 'users', component: UserComponent },
  { path: 'housekeeping', component: HousekeepingComponent },
  { path: 'groups', component: GroupComponent },
  { path: 'profile', component: MyprofileComponent },
  { path: '', component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    UserComponent,
    MyprofileComponent,
    GroupComponent,
    HousekeepingComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
